all: build

build-all-in-one:
	@docker build --tag=horon/all-in-one .

build:
	@make -C obs-server
	@make -C obs-api
	@make -C worker

minbase:
	@sudo /usr/share/docker.io/contrib/mkimage.sh -t obs/minbase \
          debootstrap --variant=minbase stable
