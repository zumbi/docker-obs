#!/usr/bin/env sh

if [ -z "$OBS_FRONTEND_HOST" ]; then
    echo >&2 'error: OBS server frontend is unavailable and hostname option'
    echo >&2 'is not specified '
    echo >&2 '  You need to specify OBS_FRONTEND_HOST'
    exit 1
fi

echo "Configure OBS frontend host: ${OBS_FRONTEND_HOST}"
sed -i s/"frontend = undef"/"frontend = '${OBS_FRONTEND_HOST}'"/g /etc/obs/BSConfig.pm

/usr/bin/supervisord -n
